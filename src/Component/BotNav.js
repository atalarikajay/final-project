import {View, Image} from 'react-native';
import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Home from '../App/Home';
import History from '../App/History';
import Logout from '../App/Logout';

//mebuat stack Screen atau Stack Navigator

const Tab = createBottomTabNavigator();

const BotNav = () => {
  return (
    <Tab.Navigator
      screenOptions={{
        headerShown: false,
      }}>
      <Tab.Screen
        name="Home"
        component={Home}
        options={{
          tabBarActiveTintColor: 'purple',
          tabBarIcon: () => (
            <View>
              <Image
                source={{
                  uri: 'https://uxwing.com/wp-content/themes/uxwing/download/web-app-development/home-button-icon.png',
                }}
                style={{
                  resizeMode: 'contain',
                  width: 30,
                  height: 30,
                  marginTop: 5,
                }}
              />
            </View>
          ),
        }}
      />
      <Tab.Screen
        name="History"
        component={History}
        options={{
          tabBarActiveTintColor: 'purple',
          tabBarIcon: () => (
            <View>
              <Image
                source={{
                  uri: 'https://cdn-icons-png.flaticon.com/512/32/32223.png',
                }}
                style={{
                  resizeMode: 'contain',
                  width: 30,
                  height: 28,
                  marginBottom: 2,
                  marginRight: 6.5,
                }}
              />
            </View>
          ),
        }}
      />
      <Tab.Screen
        name="Logout"
        component={Logout}
        options={{
          tabBarActiveTintColor: 'purple',
          tabBarIcon: () => (
            <View>
              <Image
                source={{
                  uri: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRD4CHR6qAoKknUeo6EPMxSLXhDrMUsqNMA2w&usqp=CAU',
                }}
                style={{
                  resizeMode: 'contain',
                  width: 30,
                  height: 28,
                  marginBottom: 2,
                  marginLeft: 7,
                }}
              />
            </View>
          ),
        }}
      />
    </Tab.Navigator>
  );
};

export default BotNav;
