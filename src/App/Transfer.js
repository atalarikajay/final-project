import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  Image,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import React, {useState} from 'react';
import Modal from 'react-native-modal';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import SelectDropdown from 'react-native-select-dropdown';
import LinearGradient from 'react-native-linear-gradient';
import ePayLogo from '../Img/ePayLogo.png';
import {useDispatch, useSelector} from 'react-redux';
import {sendPay} from '../redux/action/Action';

const Transfer = ({navigation}) => {
  const [dataAmount, setDataAmount] = useState('');
  const [dataSender, setDataSender] = useState('');
  const [dataTarget, setDataTarget] = useState('');
  const [dataType, setDataType] = useState('');
  const [modal, setModal] = useState(false);
  const [status, setStatus] = useState('');
  const types = ['Transfer', 'Pulsa', 'Internet'];

  const dispatch = useDispatch();
  async function Payment() {
    try {
      dispatch(sendPay(dataAmount, dataSender, dataTarget, dataType));
      setStatus('Transaksi berhasil!!');
      handleModal();
    } catch (e) {
      console.log('errors', e);
      setStatus('Maaf, Transaksi anda Gagal');
    }
  }
  const handleModal = () => {
    setModal(true);
  };
  const state = useSelector(state => state.Fetch?.Transaksi);
  console.log('ini transaksi', state);

  return (
    <SafeAreaView style={style.container}>
      <View style={style.vUtama}>
        <View style={style.vAtas}>
          <View style={style.vSelamat}>
            <Image source={ePayLogo} style={style.iLogoEpay} />
          </View>
        </View>
        <View style={style.vBawah}>
          <View style={style.vPayment}>
            <View style={style.ltrInput}>
              <View style={{flexDirection: 'row'}}>
                <Text style={style.txtSJ}>Sender </Text>
                <View style={style.txtInput}>
                  <TextInput
                    placeholder="Masukkan Nama Pengirim"
                    value={dataSender}
                    onChangeText={send => {
                      setDataSender(send);
                    }}
                    placeholderTextColor="rgba(0,0,0,0.7)"
                  />
                </View>
              </View>
              <View style={{flexDirection: 'row'}}>
                <Text style={style.txtPR}>Payment </Text>
                <View style={style.txtInType}>
                  <SelectDropdown
                    data={types}
                    onSelect={(selectedItem, index) => {
                      setDataType(selectedItem);
                      // console.log(selectedItem, index);
                    }}
                    defaultButtonText={'Payment'}
                    buttonTextAfterSelection={(selectedItem, index) => {
                      return selectedItem;
                    }}
                    rowTextForSelection={(item, index) => {
                      return item;
                    }}
                    buttonStyle={style.dropdown1BtnStyle}
                    buttonTextStyle={style.dropdown1BtnTxtStyle}
                    renderDropdownIcon={isOpened => {
                      return (
                        <FontAwesome
                          name={isOpened ? 'chevron-up' : 'chevron-down'}
                          color={'#444'}
                          size={12}
                        />
                      );
                    }}
                    dropdownIconPosition={'right'}
                    dropdownStyle={style.dropdown1DropdownStyle}
                    rowStyle={style.dropdown1RowStyle}
                    rowTextStyle={style.dropdown1RowTxtStyle}
                    value={dataType}
                  />
                </View>
              </View>
              <View style={{flexDirection: 'row'}}>
                <Text style={style.txtSJ}>Jumlah</Text>
                <View style={style.txtInput}>
                  <TextInput
                    placeholder="Masukkan Jumlah Uang"
                    value={dataAmount}
                    onChangeText={send => {
                      setDataAmount(send);
                    }}
                    placeholderTextColor="rgba(0,0,0,0.7)"
                  />
                </View>
              </View>
              <View style={{flexDirection: 'row'}}>
                <Text style={style.txtPR}>Receiver</Text>
                <View style={style.txtInput}>
                  <TextInput
                    placeholder="Masukkan Nama Penerima"
                    value={dataTarget}
                    onChangeText={send => {
                      setDataTarget(send);
                    }}
                    placeholderTextColor="rgba(0,0,0,0.7)"
                  />
                </View>
              </View>
              <View style={style.vToch}>
                <TouchableOpacity
                  onPress={() => {
                    Payment();
                  }}>
                  <LinearGradient
                    colors={['#ff512f', '#4048BF']}
                    style={style.tochKirim}>
                    <Text>Send</Text>
                  </LinearGradient>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      </View>
      <View>
        <Modal isVisible={modal} animationOut={'fadeOut'}>
          <View>
            <View style={style.wrap}>
              <Text style={style.txtSuccess}>Congratulation</Text>
              <Text style={style.txtIsi}>{status}</Text>
              <Text style={style.txtModalUser}>Sender : {dataSender}</Text>
              <Text style={style.txtModalUser}>Metode : {dataType}</Text>
              <Text style={style.txtModalUser}>Jumlah : {dataAmount}</Text>
              <Text style={style.txtModalUser}>Receiver : {dataTarget}</Text>
              <TouchableOpacity
                style={style.mainToch}
                onPress={() => (navigation.push('BotNav'), setModal(false))}>
                <Text style={style.txtToch}>Close</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      </View>
    </SafeAreaView>
  );
};

const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF5E4',
  },
  vUtama: {
    flex: 1,
    backgroundColor: 'white',
    borderRadius: 35,
    marginHorizontal: 15,
    marginVertical: 15,
  },
  vAtas: {
    width: '100%',
    height: '15%',
  },
  vBawah: {
    width: '100%',
    height: '80%',
  },
  vSelamat: {
    backgroundColor: 'white',
    marginTop: 40,
    paddingHorizontal: 15,
    paddingVertical: 5,
    elevation: 20,
    shadowColor: '#4048BF',
    borderColor: 'transparent',
    borderWidth: 1,
    borderRadius: 15,
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 1,
    shadowRadius: 6,
    alignItems: 'center',
    marginHorizontal: 80,
  },
  iLogoEpay: {
    resizeMode: 'contain',
    width: 150,
    height: 30,
    marginLeft: 8,
  },
  vPayment: {
    backgroundColor: 'gray',
    marginTop: 10,
    height: '90%',
    marginHorizontal: 10,
    borderRadius: 20,
    backgroundColor: 'white',
    elevation: 20,
    shadowColor: '#4048BF',
    borderColor: 'transparent',
    borderWidth: 1,
    borderRadius: 20,
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 1,
    shadowRadius: 6,
    marginTop: 20,
  },
  ltrInput: {
    width: 342,
    height: 490,
    alignItems: 'center',
    marginTop: Platform.OS == 'ios' ? 60 : 20,
  },
  txtInput: {
    height: Platform.OS == 'ios' ? 45 : null,
    width: 250,
    borderColor: 'gray',
    borderWidth: 0.5,
    borderRadius: 8,
    marginVertical: 15,
    justifyContent: 'center',
    marginRight: 20,
    paddingLeft: Platform.OS == 'ios' ? 16 : 10,
  },
  txtInType: {
    width: 250,
    marginVertical: 15,
    marginRight: 21,
  },
  txtSJ: {
    fontWeight: 'bold',
    marginHorizontal: 15,
    marginTop: 28,
    marginRight: 25,
  },
  txtPR: {
    fontWeight: 'bold',
    marginHorizontal: 15,
    marginTop: 28,
  },
  vToch: {
    marginLeft: Platform.OS == 'ios' ? 18 : 25,
  },
  tochKirim: {
    marginTop: 25,
    width: 170,
    height: 60,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 8,
    borderColor: 'gray',
    borderWidth: 0.3,
    elevation: 5,
    shadowOpacity: 0.5,
    shadowRadius: 0.2,
    shadowOffset: {
      height: 0.5,
      width: 0.5,
    },
  },
  dropdown1BtnStyle: {
    width: '100.5%',
    height: 45,
    backgroundColor: '#FFF',
    borderRadius: 8,
    borderWidth: 0.5,
    borderColor: 'rgba(0,0,0,0.6)',
    colorText: 'white',
    marginRight: -1,
  },
  dropdown1BtnTxtStyle: {
    color: 'rgba(0,0,0,0.7)',
    fontSize: 15,
    textAlign: 'left',
  },
  dropdown1DropdownStyle: {backgroundColor: '#EFEFEF'},
  dropdown1RowStyle: {backgroundColor: '#EFEFEF', borderBottomColor: 'gray'},
  dropdown1RowTxtStyle: {color: '#444', textAlign: 'left'},
  wrap: {
    padding: 20,
    margin: 20,
    borderRaduis: 8,
    backgroundColor: '#2D3953',
    shadowColor: '#4048BF',
    shadowOffset: {
      width: 8.4,
      height: 8.4,
    },
    shadowOpacity: 0.74,
    shadowRadius: 30,
    elevation: 50,
    borderRadius: 30,
    height: 400,
    alignItems: 'center',
  },
  txtSuccess: {
    fontSize: 40,
    color: '#ECF0F9',
    fontWeight: '600',
    fontFamily: 'Avenir',
    marginTop: 10,
    textAlign: 'center',
  },
  txtIsi: {
    fontSize: 22,
    color: '#ECF0F9',
    fontWeight: '600',
    fontFamily: 'Avenir',
    alignItems: 'center',
    textAlign: 'center',
    marginTop: 15,
  },
  txtModalUser: {
    fontSize: 18,
    color: '#ECF0F9',
    fontWeight: '600',
    fontFamily: 'Avenir',
    alignItems: 'center',
    textAlign: 'center',
    marginTop: 15,
  },
  mainToch: {
    width: 130,
    height: 55,
    borderRadius: 100,
    shadowColor: '#4048BF',
    shadowOffset: {
      width: 6.4,
      height: 6.4,
    },
    shadowOpacity: 0.5,
    shadowRadius: 20,
    backgroundColor: '#2D3953',
    borderColor: 'white',
    borderWidth: 0.5,
    elevation: 8,
    marginTop: 30,
  },
  txtToch: {
    fontSize: 22,
    color: '#ECF0F9',
    fontWeight: '600',
    fontFamily: 'Avenir',
    alignItems: 'center',
    textAlign: 'center',
    marginTop: 12,
  },
});
export default Transfer;
