import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  ScrollView,
  Image,
  TextInput,
  Platform,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import iconHstry from '../Icon/history.png';
import ePayLogo from '../Img/ePayLogo.png';
import {useDispatch, useSelector} from 'react-redux';
import {getHist} from '../redux/action/Action';

const History = () => {
  const [search, setSearch] = useState('');
  const dispatch = useDispatch();
  const Histori = async () => {
    try {
      dispatch(getHist());
    } catch (e) {
      console.log(e);
    }
  };
  const states = useSelector(state => state.Fetchs?.History);
  const hasil = Object.entries(states);

  useEffect(() => {
    Histori();
  }, []);

  return (
    <SafeAreaView style={style.container}>
      <View style={style.vUtama}>
        <View style={style.vAtas}>
          <View style={style.vSelamat}>
            <Image source={ePayLogo} style={style.iLogoEpay} />
          </View>
          <View style={style.vSearch}>
            <TextInput
              placeholder="Search"
              style={style.txtInSearch}
              placeholderTextColor="black"
              value={search}
              onChangeText={e => {
                setSearch(e);
              }}
            />
          </View>
        </View>
        <View style={style.vBawah}>
          <ScrollView
            contentContainerStyle={style.scroll}
            showsVerticalScrollIndicator={false}>
            {hasil?.map((e, index) => {
              const dataFilter = e.filter(item => {
                return (
                  item?.type?.toLowerCase().includes(search.toLowerCase()) ||
                  item?.amount
                    ?.toString()
                    .toLowerCase()
                    .includes(search.toLowerCase()) ||
                  item?.sender
                    ?.toString()
                    .toLowerCase()
                    .includes(search.toLowerCase())
                );
              });
              return (
                <View key={index}>
                  {dataFilter.map((es, index) => {
                    return (
                      <View style={style.vHstry}>
                        <View>
                          <View>
                            <View style={style.vSJ}>
                              <Text style={style.txtSJM}>
                                Sender : {e[1].sender}
                              </Text>
                            </View>
                            <View style={style.vSJ}>
                              <Text style={style.txtSJM}>
                                Jumlah : Rp. {e[1].amount}
                              </Text>
                            </View>
                            <View style={style.vSJ}>
                              <Text style={style.txtSJM}>
                                Metode : Rp. {e[1].type}
                              </Text>
                            </View>
                            <View style={style.vIcType}>
                              <Text style={style.txtSJM}>
                                Receiver : {e[1].target}
                              </Text>
                              <Image source={iconHstry} style={style.iHstry} />
                            </View>
                          </View>
                        </View>
                      </View>
                    );
                  })}
                </View>
              );
            })}
          </ScrollView>
        </View>
      </View>
    </SafeAreaView>
  );
};
const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF5E4',
  },
  vUtama: {
    flex: 1,
    backgroundColor: 'white',
    borderRadius: 35,
    marginHorizontal: 15,
    marginVertical: 15,
  },
  vAtas: {
    width: '100%',
    height: '20%',
  },
  vBawah: {
    marginTop: 5,
    width: '100%',
    height: '78.6%',
  },
  vSelamat: {
    backgroundColor: 'white',
    marginTop: 13,
    paddingHorizontal: 15,
    paddingVertical: 5,
    elevation: 20,
    shadowColor: '#4048BF',
    borderColor: 'transparent',
    borderWidth: 1,
    borderRadius: 15,
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 1,
    shadowRadius: 6,
    alignItems: 'center',
    marginHorizontal: 80,
  },
  iLogoEpay: {
    resizeMode: 'contain',
    width: 150,
    height: 30,
    marginLeft: 8,
  },
  vSearch: {
    marginTop: Platform.OS == 'ios' ? 18 : 15,
  },
  txtInSearch: {
    marginHorizontal: 20,
    paddingVertical: Platform.OS == 'ios' ? 13 : 5,
    borderRadius: 10,
    backgroundColor: 'white',
    borderWidth: 1,
    borderColor: 'gray',
    paddingLeft: 10,
  },
  vHstry: {
    backgroundColor: 'white',
    elevation: 5,
    shadowColor: 'black',
    shadowOpacity: 0.2,
    shadowRadius: 4,
    shadowOffset: {
      height: 1,
      width: 2,
    },
    marginVertical: 10,
    borderRadius: 10,
  },
  scroll: {
    padding: 20,
  },
  vIcType: {
    marginLeft: 15,
    marginTop: 15,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  iHstry: {
    width: 50,
    height: 40,
    bottom: 14,
    resizeMode: 'contain',
  },
  vSJ: {
    marginLeft: 15,
    marginTop: 15,
  },
  txtSJM: {
    fontWeight: '700',
  },
});
export default History;
