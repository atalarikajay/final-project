import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  Platform,
  Image,
  TouchableOpacity,
} from 'react-native';
import {SliderBox} from 'react-native-image-slider-box';
import React from 'react';
import iconTf from '../Icon/tf.png';
import iconByr from '../Icon/bayar1.png';
import iconeWallet from '../Icon/eWallet.png';
import iconTopUp from '../Icon/TopUp.png';
import iconEtc from '../Icon/etc.png';
import iconQr from '../Icon/Qr.png';
import ePayLogo from '../Img/ePayLogo.png';

const Home = ({navigation}) => {
  const images = [
    require('../Img/iklan1.jpeg'),
    require('../Img/iklan2.jpeg'),
    require('../Img/iklan3.jpeg'),
    require('../Img/iklan4.jpeg'),
  ];

  return (
    <SafeAreaView style={style.container}>
      <View style={style.vUtama}>
        <View style={style.vAtas}>
          <View style={style.vWelcome}>
            <View style={style.vSelamat}>
              <Image source={ePayLogo} style={style.iLogoEpay} />
            </View>
            <Text style={style.txtMau}>
              Mau Transfer, Top Up dan Bayar Lebih Cepat ?
            </Text>
            <Text style={style.txtePay}>Ya pakai ePay Ajahh !!!</Text>
            <Text style={style.txtSaldo}>Saldo Rp. 2.500.000</Text>
          </View>
          <View style={style.vMenu}>
            <View style={style.vIcon}>
              <View>
                <TouchableOpacity
                  onPress={() => {
                    navigation.navigate('Transfer');
                  }}>
                  <Image source={iconTf} />
                  <Text style={style.txtTf}>Transfer</Text>
                </TouchableOpacity>
              </View>
              <View>
                <Image source={iconByr} style={style.iByr} />
                <Text style={style.txtByr}>Bayar</Text>
              </View>
              <View>
                <Image source={iconeWallet} />
                <Text style={style.txteWallet}>e-Wallet</Text>
              </View>
            </View>
            <View style={style.vIcon2}>
              <View>
                <Image source={iconQr} style={style.iQr} />
                <Text style={style.txtQr}>QR</Text>
              </View>
              <View>
                <Image source={iconTopUp} style={style.iTopUp} />
                <Text style={style.txtTopUp}>Top Up</Text>
              </View>
              <View>
                <Image source={iconEtc} style={style.iEtc} />
                <Text style={style.txtEtc}>Lainnya</Text>
              </View>
            </View>
          </View>
        </View>
        <View style={style.vBawah}>
          <SliderBox
            images={images}
            dotColor="black"
            inactiveDotColor="white"
            autoplay={true}
            autoplayInterval={3000}
            circleLoop={true}
            ImageComponentStyle={style.imageSlide}
          />
        </View>
      </View>
    </SafeAreaView>
  );
};
const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF5E4',
  },
  vUtama: {
    flex: 1,
    backgroundColor: 'white',
    borderRadius: 35,
    marginHorizontal: 15,
    marginVertical: 15,
  },
  vAtas: {
    width: '100%',
    height: '70%',
  },
  vBawah: {
    width: '100%',
    height: '40%',
  },
  vWelcome: {
    width: '100%',
    height: '45%',
    alignItems: 'center',
  },
  iLogoEpay: {
    resizeMode: 'contain',
    width: 150,
    height: 30,
    marginLeft: 8,
  },
  vMenu: {
    backgroundColor: 'white',
    width: '100%',
    height: Platform.OS == 'ios' ? '50%' : '53.5%',
    elevation: 20,
    shadowColor: '#4048BF',
    borderColor: 'transparent',
    borderWidth: 1,
    borderRadius: 15,
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 1,
    shadowRadius: 6,
  },
  vSelamat: {
    backgroundColor: 'white',
    marginTop: 13,
    paddingHorizontal: 15,
    paddingVertical: 5,
    elevation: 20,
    shadowColor: '#4048BF',
    borderColor: 'transparent',
    borderWidth: 1,
    borderRadius: 15,
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 1,
    shadowRadius: 6,
  },
  txtSelamat: {
    marginTop: 2,
    fontSize: 24,
    fontWeight: 'bold',
    color: 'black',
  },
  txtMau: {
    marginTop: Platform.OS == 'ios' ? 25 : 15,
    fontSize: 16,
    color: 'black',
    fontWeight: '500',
  },
  txtePay: {
    marginTop: Platform.OS == 'ios' ? 15 : 13,
    fontSize: 16,
    color: 'black',
    fontWeight: '500',
  },
  txtSaldo: {
    marginTop: Platform.OS == 'ios' ? 42 : 21,
    marginLeft: Platform.OS == 'ios' ? 195 : 228,
    fontSize: 16,
    color: 'black',
    fontWeight: 'bold',
  },
  vIcon: {
    flexDirection: 'row',
    marginTop: Platform.OS == 'ios' ? 30 : 15,
    justifyContent: 'space-evenly',
  },
  vIcon2: {
    flexDirection: 'row',
    marginTop: Platform.OS == 'ios' ? 20 : 17,
    justifyContent: 'space-evenly',
    marginRight: 8,
  },
  txtTf: {
    marginTop: 10,
    marginLeft: 3,
    color: 'black',
    fontWeight: '500',
  },
  iByr: {
    width: 50,
    height: 60,
  },
  txtByr: {
    marginTop: 14,
    marginLeft: 6,
    color: 'black',
    fontWeight: '500',
  },
  txteWallet: {
    marginTop: 10,
    marginLeft: 6,
    color: 'black',
    fontWeight: '500',
  },
  iQr: {
    width: 55,
    height: 60,
    marginLeft: 8,
  },
  txtQr: {
    marginTop: 1,
    marginLeft: 24,
    color: 'black',
    fontWeight: '500',
  },
  iTopUp: {
    width: 50,
    height: 60,
    marginLeft: 14,
  },
  txtTopUp: {
    marginTop: 1.5,
    marginLeft: 17,
    color: 'black',
    fontWeight: '500',
  },
  iEtc: {
    marginLeft: 6,
  },
  txtEtc: {
    bottom: 4,
    marginLeft: 14,
    color: 'black',
    fontWeight: '500',
  },
  imageSlide: {
    borderRadius: Platform.OS == 'ios' ? 20 : 20,
    height: Platform.OS == 'ios' ? 200 : 170,
    width: Platform.OS == 'ios' ? 362 : 383,
    marginTop: 5,
    marginRight: Platform.OS == 'ios' ? 32 : 30,
  },
});
export default Home;
