import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import Modal from 'react-native-modal';
import {useDispatch} from 'react-redux';

const Logout = ({navigation}) => {
  const [modal, setModal] = useState(true);
  const dispatch = useDispatch();
  async function Logoutt() {
    try {
      console.log('logout');
      dispatch(Logout());
      await AsyncStorage.removeItem('Token');
    } catch (e) {
      console.log('error', e);
    }
  }

  return (
    <SafeAreaView style={style.container}>
      <View>
        <Modal isVisible={modal} animationOut={'fadeOut'}>
          <View>
            <View style={style.wrap}>
              <Text style={style.txtHello}>Logout</Text>
              <Text style={style.txtIsi}>Yakin Mau Logout ??</Text>
              <View style={style.vToch}>
                <View>
                  <TouchableOpacity
                    style={style.mainToch}
                    onPress={() => (
                      navigation.push('BotNav'), setModal(false)
                    )}>
                    <Text style={style.txtToch}>Back</Text>
                  </TouchableOpacity>
                </View>
                <View>
                  <TouchableOpacity
                    style={style.mainToch}
                    onPress={async () => {
                      Logoutt();
                      navigation.push('Login');
                      setModal(false);
                    }}>
                    <Text style={style.txtToch}>Logout</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </View>
        </Modal>
      </View>
    </SafeAreaView>
  );
};

const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },
  wrap: {
    padding: 20,
    margin: 20,
    borderRaduis: 8,
    backgroundColor: '#2D3953',
    shadowColor: '#4048BF',
    shadowOffset: {
      width: 8.4,
      height: 8.4,
    },
    shadowOpacity: 0.74,
    shadowRadius: 30,
    elevation: 50,
    borderRadius: 30,
  },
  txtHello: {
    fontSize: 51.2,
    color: '#ECF0F9',
    fontWeight: '600',
    fontFamily: 'Avenir',
    marginTop: 20,
    textAlign: 'center',
  },
  txtIsi: {
    fontSize: 28.8,
    color: '#ECF0F9',
    fontWeight: '600',
    fontFamily: 'Avenir',
    alignItems: 'center',
    textAlign: 'center',
    marginTop: 30,
  },
  vToch: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 70,
  },
  mainToch: {
    width: 130,
    height: 55,
    borderRadius: 100,
    shadowColor: '#4048BF',
    shadowOffset: {
      width: 6.4,
      height: 6.4,
    },
    shadowOpacity: 0.5,
    shadowRadius: 20,
    backgroundColor: '#2D3953',
    borderColor: 'white',
    borderWidth: 0.5,
    elevation: 8,
  },
  txtToch: {
    fontSize: 22,
    color: '#ECF0F9',
    fontWeight: '600',
    fontFamily: 'Avenir',
    alignItems: 'center',
    textAlign: 'center',
    marginTop: 12,
  },
});

export default Logout;
