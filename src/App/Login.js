import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  TextInput,
  Image,
  Platform,
  BackHandler,
  TouchableOpacity,
} from 'react-native';
import Modal from 'react-native-modal';
import LinearGradient from 'react-native-linear-gradient';
import React, {useEffect, useState} from 'react';
import {useDispatch} from 'react-redux';
import {Loginn} from '../redux/action/Action';

const Login = ({navigation}) => {
  const HandlerBack = () => {
    navigation.goBack();
    return true;
  };

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', HandlerBack);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', HandlerBack);
    };
  }, []);

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [disables, setDisables] = useState(false);
  const [modal, setModal] = useState(false);
  const dispatch = useDispatch();

  const ValidatorButton = () => {
    return email !== '' && password !== ''
      ? setDisables(false)
      : setDisables(true);
  };

  useEffect(() => {
    ValidatorButton();
  }, [email, password]);

  const validateEmail = () => {
    if (email === 'Ajay@gmail.com') {
      console.log(email);
      return true;
    } else {
      console;
      if (email === '') {
        console.error('Error', 'Email tidak boleh kosong');
        return false;
      }
      if (email !== setEmail) {
        console.error('Error', 'Email tidak valid');
        return false;
      }
    }
    return false;
  };

  const validatePassword = () => {
    if (password === 'zxcasd123') {
      return true;
    } else {
      console;
      if (password === '') {
        console.error('Error', 'Password tidak boleh kosong');
        return false;
      }
      if (password !== setPassword) {
        console.error('Error', 'Password tidak valid');
        return false;
      }
    }
    return false;
  };

  async function Loginnn(e) {
    try {
      dispatch(Loginn(email, password));
    } catch (e) {
      console.log('error', e);
    }
  }

  const handleSubmit = () => {
    if (validateEmail() && validatePassword()) {
      Loginnn();
      console.log(email, password);
      handleModal(true);
    }
  };
  const handleModal = () => {
    setModal(true);
  };

  return (
    <SafeAreaView style={style.container}>
      <View style={style.head}>
        <View style={style.vGambar}>
          <Image
            style={style.gambar}
            source={{
              uri: 'https://images.vexels.com/media/users/3/142789/isolated/preview/2bfb04ad814c4995f0c537c68db5cd0b-multicolor-swirls-circle-logo.png',
            }}
          />
        </View>
        <View style={style.txtEmail}>
          <TextInput
            placeholder="Masukkan Email Anda"
            placeholderTextColor={'rgba(0,0,0,0.5)'}
            value={email}
            onChangeText={e => {
              setEmail(e);
            }}
          />
        </View>
        <View style={style.txtPass}>
          <TextInput
            placeholder="Masukkan Password Anda"
            placeholderTextColor={'rgba(0,0,0,0.5)'}
            secureTextEntry={true}
            value={password}
            onChangeText={e => {
              setPassword(e);
            }}
          />
        </View>
        <View>
          <TouchableOpacity
            onPress={() => {
              handleSubmit();
            }}>
            <LinearGradient
              colors={['#ff512f', '#4048BF']}
              style={style.tochLogin}>
              <Text style={style.txtLogin}>LOGIN</Text>
            </LinearGradient>
          </TouchableOpacity>
        </View>
        <View style={style.vDaftar}>
          <Text style={style.txtAkun}>Belum Punya Akun ?</Text>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('Regis');
            }}>
            <Text style={style.txtDaftar}>Daftar Yuk !</Text>
          </TouchableOpacity>
        </View>
      </View>
      <View>
        <Modal isVisible={modal} animationOut={'fadeOut'}>
          <View>
            <View style={style.wrap}>
              <Text style={style.txtSuccess}>Success</Text>
              <Text style={style.txtIsi}>Selamat Datang !! {email}</Text>

              <TouchableOpacity
                style={style.mainToch}
                onPress={() => (navigation.push('BotNav'), setModal(false))}>
                <Text style={style.txtToch}>Close</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      </View>
    </SafeAreaView>
  );
};

const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF5E4',
  },
  head: {
    paddingHorizontal: 50,
  },
  vGambar: {
    paddingVertical: 20,
    marginHorizontal: 50,
    marginTop: 65,
  },
  gambar: {
    resizeMode: 'contain',
    width: '100%',
    height: 180,
  },
  txtEmail: {
    height: Platform.OS == 'ios' ? 50 : 42,
    justifyContent: 'center',
    marginTop: 55,
    borderWidth: 0.3,
    borderRadius: 8,
    borderColor: 'gray',
    paddingHorizontal: 8,
  },
  txtPass: {
    height: Platform.OS == 'ios' ? 50 : 42,
    justifyContent: 'center',
    marginTop: 25,
    borderWidth: 0.3,
    borderRadius: 8,
    borderColor: 'gray',
    paddingHorizontal: 8,
  },
  tochLogin: {
    marginTop: 40,
    height: 45,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 8,
    marginHorizontal: Platform.OS == 'ios' ? 43 : 55,
  },
  txtLogin: {
    fontSize: 18,
    color: 'white',
    fontWeight: 'bold',
  },
  vDaftar: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 15,
  },
  txtAkun: {
    marginHorizontal: 5,
  },
  txtDaftar: {
    textDecorationLine: 'underline',
    color: 'blue',
  },
  wrap: {
    padding: 20,
    margin: 20,
    borderRaduis: 8,
    backgroundColor: '#2D3953',
    shadowColor: '#4048BF',
    shadowOffset: {
      width: 8.4,
      height: 8.4,
    },
    shadowOpacity: 0.74,
    shadowRadius: 30,
    elevation: 50,
    borderRadius: 30,
    height: 300,
    alignItems: 'center',
  },
  txtSuccess: {
    fontSize: 40,
    color: '#ECF0F9',
    fontWeight: '600',
    fontFamily: 'Avenir',
    marginTop: 10,
    textAlign: 'center',
  },
  txtIsi: {
    fontSize: 22,
    color: '#ECF0F9',
    fontWeight: '600',
    fontFamily: 'Avenir',
    alignItems: 'center',
    textAlign: 'center',
    marginTop: 30,
  },
  mainToch: {
    width: 130,
    height: 55,
    borderRadius: 100,
    shadowColor: '#4048BF',
    shadowOffset: {
      width: 6.4,
      height: 6.4,
    },
    shadowOpacity: 0.5,
    shadowRadius: 20,
    backgroundColor: '#2D3953',
    borderColor: 'white',
    borderWidth: 0.5,
    elevation: 8,
    marginTop: 40,
  },
  txtToch: {
    fontSize: 22,
    color: '#ECF0F9',
    fontWeight: '600',
    fontFamily: 'Avenir',
    alignItems: 'center',
    textAlign: 'center',
    marginTop: 12,
  },
});

export default Login;
