import {View, SafeAreaView, StyleSheet, Image} from 'react-native';
import React, {useEffect} from 'react';
import ePay from '../Img/ePay.gif';

const SplashScreen = ({navigation}) => {
  useEffect(() => {
    setTimeout(() => {
      navigation.replace('Login');
    }, 3000);
  });

  return (
    <SafeAreaView style={style.container}>
      <View style={style.vUtama}>
        <View>
          <Image source={ePay} style={style.gambar} />
        </View>
        <View>
          <Image
            style={style.gambar2}
            source={{
              uri: 'https://images.vexels.com/media/users/3/142789/isolated/preview/2bfb04ad814c4995f0c537c68db5cd0b-multicolor-swirls-circle-logo.png',
            }}
          />
        </View>
      </View>
    </SafeAreaView>
  );
};

const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF5E4',
    alignItems: 'center',
    justifyContent: 'center',
  },
  vUtama: {
    width: 250,
    height: 250,
    marginTop: 65,
  },
  gambar: {
    resizeMode: 'contain',
    width: '100%',
    height: 150,
    marginTop: 50,
  },
  gambar2: {
    resizeMode: 'contain',
    width: '100%',
    height: 130,
    marginTop: -230,
  },
});
export default SplashScreen;
