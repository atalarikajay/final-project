import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  TextInput,
  TouchableOpacity,
  Image,
  Platform,
} from 'react-native';
import Modal from 'react-native-modal';
import LinearGradient from 'react-native-linear-gradient';
import React, {useEffect, useState} from 'react';
import {sendRegis} from '../redux/action/Action';
import {useDispatch, useSelector} from 'react-redux';

const Regis = ({navigation}) => {
  const [username, setUsername] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [disables, setDisables] = useState(false);
  const [modal, setModal] = useState(false);
  const dispatch = useDispatch();

  const ValidatorButton = () => {
    return username !== '' && email !== '' && password !== ''
      ? setDisables(false)
      : setDisables(true);
  };
  useEffect(() => {
    ValidatorButton();
  }, [username, email, password]);
  const validateUsername = () => {
    if (username === '') {
      console.error('Error', 'Username tidak boleh kosong');
      return false;
    }
    return true;
  };

  const validateEmail = () => {
    if (email === '') {
      console.error('Error', 'Email tidak boleh kosong');
      return false;
    }
    if (!/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)) {
      console.error('Error', 'Email tidak valid');
      return false;
    }
    return true;
  };

  const validatePassword = () => {
    if (password === '') {
      console.error('Error', 'Password tidak boleh kosong');
      return false;
    }
    if (password.length < 8) {
      console.error('Error', 'Password minimal 8 karakter');
      return false;
    }
    if (!/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/.test(password)) {
      console.error(
        'Password harus terdiri dari huruf besar, huruf kecil, dan angka',
      );
      return false;
    }
    return true;
  };

  async function Register() {
    try {
      dispatch(sendRegis(username, email, password));
    } catch (e) {
      console.log('error', e);
    }
  }
  const state = useSelector(state => state.Auth);
  console.log('ini Regis', state);

  const handleRegis = () => {
    if (validateUsername() && validateEmail() && validatePassword()) {
      Register();
      setModal(true);
    }
  };

  return (
    <SafeAreaView style={style.container}>
      <View style={style.head}>
        <View style={style.vBack}>
          <TouchableOpacity
            onPress={() => {
              navigation.goBack();
            }}>
            <Image
              style={style.back}
              source={{
                uri: 'https://cdn-icons-png.flaticon.com/512/93/93634.png',
              }}
            />
          </TouchableOpacity>
        </View>
        <View style={style.vGambar}>
          <Image
            style={style.gambar}
            source={{
              uri: 'https://images.vexels.com/media/users/3/142789/isolated/preview/2bfb04ad814c4995f0c537c68db5cd0b-multicolor-swirls-circle-logo.png',
            }}
          />
        </View>
        <View style={style.txtUsername}>
          <TextInput
            placeholder="Masukkan Username Anda"
            placeholderTextColor={'rgba(0,0,0,0.5)'}
            onChangeText={send => {
              setUsername(send);
            }}
          />
        </View>
        <View style={style.txtPass}>
          <TextInput
            placeholder="Masukkan Email Anda"
            placeholderTextColor={'rgba(0,0,0,0.5)'}
            onChangeText={send => {
              setEmail(send);
            }}
          />
        </View>
        <View style={style.txtPass}>
          <TextInput
            placeholder="Masukkan Password Anda"
            placeholderTextColor={'rgba(0,0,0,0.5)'}
            secureTextEntry={true}
            onChangeText={send => {
              setPassword(send);
            }}
          />
        </View>
        <View>
          <TouchableOpacity
            onPress={() => {
              handleRegis();
            }}>
            <LinearGradient
              colors={['#ff512f', '#4048BF']}
              style={style.tochSubmit}>
              <Text style={style.txtLogin}>SUBMIT</Text>
            </LinearGradient>
          </TouchableOpacity>
        </View>
      </View>
      <View>
        <Modal isVisible={modal} animationOut={'fadeOut'}>
          <View>
            <View style={style.wrap}>
              <Text style={style.txtSuccess}>Success</Text>
              <Text style={style.txtIsi}>Selamat Datang !!</Text>
              <Text style={style.txtModalUser}>{username}</Text>
              <TouchableOpacity
                style={style.mainToch}
                onPress={() => (navigation.push('BotNav'), setModal(false))}>
                <Text style={style.txtToch}>Close</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      </View>
    </SafeAreaView>
  );
};

const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFF5E4',
  },
  head: {
    paddingHorizontal: 50,
  },
  vBack: {
    marginTop: Platform.OS == 'ios' ? null : 8,
    marginLeft: -40,
    width: 60,
  },
  back: {
    width: 40,
    height: 40,
  },
  vGambar: {
    marginTop: 70,
  },
  gambar: {
    resizeMode: 'contain',
    width: '100%',
    height: 200,
  },
  txtUsername: {
    height: Platform.OS == 'ios' ? 50 : 42,
    justifyContent: 'center',
    marginTop: Platform.OS == 'ios' ? 55 : 20,
    borderWidth: 0.3,
    borderRadius: 8,
    borderColor: 'gray',
    paddingHorizontal: 8,
  },
  txtPass: {
    height: Platform.OS == 'ios' ? 50 : 42,
    justifyContent: 'center',
    marginTop: 20,
    borderWidth: 0.3,
    borderRadius: 8,
    borderColor: 'gray',
    paddingHorizontal: 8,
  },
  tochSubmit: {
    marginTop: Platform.OS == 'ios' ? 35 : 30,
    height: 45,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 8,
    marginHorizontal: Platform.OS == 'ios' ? 47 : 65,
  },
  txtLogin: {
    fontSize: 18,
    color: 'white',
    fontWeight: 'bold',
  },
  wrap: {
    padding: 20,
    margin: 20,
    borderRaduis: 8,
    backgroundColor: '#2D3953',
    shadowColor: '#4048BF',
    shadowOffset: {
      width: 8.4,
      height: 8.4,
    },
    shadowOpacity: 0.74,
    shadowRadius: 30,
    elevation: 50,
    borderRadius: 30,
    height: 300,
    alignItems: 'center',
  },
  txtSuccess: {
    fontSize: 40,
    color: '#ECF0F9',
    fontWeight: '600',
    fontFamily: 'Avenir',
    marginTop: 10,
    textAlign: 'center',
  },
  txtIsi: {
    fontSize: 22,
    color: '#ECF0F9',
    fontWeight: '600',
    fontFamily: 'Avenir',
    alignItems: 'center',
    textAlign: 'center',
    marginTop: 30,
  },
  txtModalUser: {
    fontSize: 18,
    color: '#ECF0F9',
    fontWeight: '600',
    fontFamily: 'Avenir',
    alignItems: 'center',
    textAlign: 'center',
    marginTop: 15,
  },
  mainToch: {
    width: 130,
    height: 55,
    borderRadius: 100,
    shadowColor: '#4048BF',
    shadowOffset: {
      width: 6.4,
      height: 6.4,
    },
    shadowOpacity: 0.5,
    shadowRadius: 20,
    backgroundColor: '#2D3953',
    borderColor: 'white',
    borderWidth: 0.5,
    elevation: 8,
    marginTop: 30,
  },
  txtToch: {
    fontSize: 22,
    color: '#ECF0F9',
    fontWeight: '600',
    fontFamily: 'Avenir',
    alignItems: 'center',
    textAlign: 'center',
    marginTop: 12,
  },
});

export default Regis;
