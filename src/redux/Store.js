import {createStore, applyMiddleware} from 'redux';
import rootReducer from './reducer/Index';
import thunk from 'redux-thunk';

const storeRedux = createStore(rootReducer, applyMiddleware(thunk));

export default storeRedux;
