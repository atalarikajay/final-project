const initialState = {
  DataFetch: null, //null merupakan tipe data, digunakan sesuai dengan kebutuhan
  History: [],
  Transaksi: [],
};

const Fetch = (state = initialState, action) => {
  switch (
    action.type // kenapa .type karena tipe apa saja yang dibutuhkan
  ) {
    case 'TRANSAKSI':
      return {
        ...state,
        Transaksi: action.data,
      };
    case 'HISTORY':
      return {
        ...state,
        History: action.data,
      };
    default:
      return state;
  }
};

export default Fetch;
