const initialState = {
  DataAuth: null, //null merupakan tipe data, digunakan sesuai dengan kebutuhan
};

const Auth = (state = initialState, action) => {
  switch (
    action.type // kenapa .type karena tipe apa saja yang dibutuhkan
  ) {
    case 'LOGIN':
      return {
        ...state,
        AuthData: action.data,
      };
    case 'REGIS':
      return {
        ...state,
        DataAuth: action.data,
      };
    case 'LOGOUT':
      return {
        ...state,
        AuthData: action.data,
      };
    default:
      return state;
  }
};

export default Auth;
