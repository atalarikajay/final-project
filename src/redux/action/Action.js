import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';

export const Loginn = (email, password) => {
  const dataToken = async value => {
    await AsyncStorage.setItem('Token', value);
  };

  return dispatch => {
    const data = {
      email,
      password,
    };
    dispatch({
      type: 'LOGIN',
      data: data,
    });
    dataToken(JSON.stringify(data));
  };
};

export const sendRegis = (username, email, password) => {
  const dataToken = async value => {
    await AsyncStorage.setItem('Token', value);
  };
  return dispatch => {
    var data = JSON.stringify({
      username: username,
      email: email,
      password: password,
    });

    var config = {
      method: 'POST',
      maxBodyLength: Infinity,
      url: 'https://test-app-49a79-default-rtdb.asia-southeast1.firebasedatabase.app/users.json',
      data: data,
    };

    axios(config)
      .then(function (response) {
        console.log(JSON.stringify(response.data));
        dataToken(JSON.stringify(response.data));
        const data = JSON.stringify(response.data);
        dispatch({
          type: 'REGIS',
          data: data,
        });
      })
      .catch(function (error) {
        console.log(error);
      });
  };
};

export const sendPay = (dataAmount, dataSender, dataTarget, dataType) => {
  const dataToken = async value => {
    await AsyncStorage.setItem('Token', value);
  };
  return dispatch => {
    var data = JSON.stringify({
      amount: dataAmount,
      sender: dataSender,
      target: dataTarget,
      type: dataType,
    });

    var config = {
      method: 'POST',
      maxBodyLength: Infinity,
      url: 'https://test-app-49a79-default-rtdb.asia-southeast1.firebasedatabase.app/transaction.json',
      data: data,
    };

    axios(config)
      .then(function (response) {
        console.log(JSON.stringify(response.data));
        dataToken(JSON.stringify(response.data));
        const data = JSON.stringify(response.data);
        dispatch({
          type: 'TRANSAKSI',
          data: data,
        });
      })
      .catch(function (error) {
        console.log(error);
      });
  };
};

export const getHist = () => {
  const dataToken = async value => {
    await AsyncStorage.setItem('Token', value);
  };
  return dispatch => {
    var data = '';

    var config = {
      method: 'get',
      maxBodyLength: Infinity,
      url: 'https://test-app-49a79-default-rtdb.asia-southeast1.firebasedatabase.app/transaction.json',
      data: data,
    };

    axios(config)
      .then(function (response) {
        console.log(JSON.stringify(response.data));
        const data = response.data;
        dispatch({
          type: 'HISTORY',
          data: data,
        });
        dataToken(JSON.stringify(response.data));
      })
      .catch(function (error) {
        console.log(error);
      });
  };
};

export const Logout = () => {
  return dispatch => {
    const data = '';
    dispatch({
      type: 'LOGOUT',
      data: data,
    });
  };
};
