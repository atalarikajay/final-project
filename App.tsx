import React from 'react';
import Route from './route/Route';
import {NavigationContainer} from '@react-navigation/native';
import {Provider} from 'react-redux';
import storeRedux from './src/redux/Store';

const App = () => {
  return (
    <Provider store={storeRedux}>
      <NavigationContainer independent={true}>
        <Route />
      </NavigationContainer>
    </Provider>
  );
};

export default App;
